package Main;

import javax.xml.ws.*;
// import WS.DemoImplements;
import WS.ProductsImplements;

public class Main {

	public static void main(String[] args) {

		try {
			// Endpoint.publish("http://127.0.0.1:4789/ws/demo", new DemoImplements());
			Endpoint.publish("http://127.0.0.1:4800/ws/products", new ProductsImplements());
			System.out.println("Done");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
