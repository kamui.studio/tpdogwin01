package WS;

import javax.jws.*;

@WebService(endpointInterface = "WS.Demo")
public class DemoImplements implements Demo {

	public String helloWorld() {
		return "Hello World!";
	}
	
	public String hi(String fullName) {
		return "Hi, " + fullName;
	}
	
}