package WS;

import java.util.List;

import javax.jws.*;

import Entities.Products;

@WebService
public interface IProduct {

		@WebMethod
		public Products findById(int uid);
		
		@WebMethod
		public List<Products> findAll();
	
}
