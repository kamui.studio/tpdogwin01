package WS;

import java.util.List;
import javax.jws.*;
import DAO.DAOProducts;
import Entities.Products;

@WebService(endpointInterface = "WS.IProduct")
public class ProductsImplements implements IProduct {

	private DAOProducts d = new DAOProducts();
	
	public Products findById(int uid) {
		return d.findById(uid);
	}

	public List<Products> findAll() {
		return d.findAll();
	}

}