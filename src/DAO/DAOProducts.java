package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import Entities.Products;

public class DAOProducts implements DAO<Products, Integer> {

	static private String DBHOST = "jdbc:mysql://127.0.0.1/AJC";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	static private String PREFIX = "ws_";
	
	// []

	public void create(Products p) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `" + PREFIX + "students` (`id_house`, `last_name`, `first_name`, `avatar`, `age`) VALUES (?, ?, ?, ?, ?);");
			ps.setInt(1, s.getId_house());
			ps.setString(2, s.getLast_name());
			ps.setString(3, s.getFirst_name());
			ps.setString(4, s.getAvatar());
			ps.setInt(5, s.getAge());
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/

	}

	// []
	public Products findById(Integer uid) {

		Products p = new Products();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT `" + PREFIX + "products`.* FROM `" + PREFIX + "products` WHERE `" + PREFIX + "products`.`id`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				p = new Products(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getDouble("price"));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return p;

	}

	// []
	public List<Products> findAll() {

		List<Products> Products = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `" + PREFIX + "products` WHERE 1");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				Products.add(new Products(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getDouble("price")));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return Products;
	}



	// []
	public void update(Products p) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `" + PREFIX + "students` SET `first_name`=?, `last_name`=?, `age`=? WHERE 1 AND `id`=?");
			ps.setString(1, c.getFirst_name());
			ps.setString(2, c.getLast_name());
			ps.setInt(3, c.getAge());
			ps.setInt(4, c.getId());
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
		
	}


	// []
	public void delete(Integer uid) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `" + PREFIX + "students` WHERE `" + PREFIX + "students`.`id`=?");
			ps.setInt(1, uid);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
	
	}

}