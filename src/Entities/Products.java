package Entities;

public class Products {

	private int uid;
	private String name;
	private String desc;
	private double price;

	public Products(int uid, String name, String desc, double price) {
		this.uid = uid;
		this.name = name;
		this.desc = desc;
		this.price = price;
	}

	public Products() {
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return "Products [uid=" + uid + ", name=" + name + ", desc=" + desc + ", price=" + price + "]";
	}
	
}
